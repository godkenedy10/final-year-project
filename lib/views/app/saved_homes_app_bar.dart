import 'package:flutter/material.dart';

class SavedHomesAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppBar appBar;

  const SavedHomesAppBar({Key key, @required this.appBar}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.tealAccent,
      title: Text(
        'Saved Homes',
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
      ),
      centerTitle: true,
      actions: <Widget>[
        IconButton(
            icon: Icon(
              Icons.share,
              color: Colors.black,
            ),
            onPressed: () {})
      ],
      bottom: PreferredSize(
          child: Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              child: Row(children: <Widget>[
                Expanded(
                  child: InkWell(
                    onTap: () {},
                    child: Container(
                      height: 40,
                      child: Text(
                        'Sort by: Saved Date',
                        style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.w500,
                            color: Colors.black),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Text(
                    'Filter',
                    style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                )
              ])),
          preferredSize: Size(MediaQuery.of(context).size.width, 50)),
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
}
