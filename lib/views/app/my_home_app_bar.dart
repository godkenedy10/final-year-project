import 'package:flutter/material.dart';
import 'package:fyp/views/screens/landlord_review_screen.dart';

class MyHomeAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppBar appBar;

  const MyHomeAppBar({Key key, @required this.appBar}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
       backgroundColor: Colors.tealAccent,
      title: Text(
        "My Home",
        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
      ),
      centerTitle: true,
      actions: [
        InkWell(
            onTap: () {
               Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                LandlordReviewScreen()));
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 15,right: 20),
              child: Text(
                "Edit",
                style: TextStyle(fontWeight: FontWeight.w500,fontSize: 20,color: Colors.black
                
                ),
              ),
            )),
      ],
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
}
