import 'package:flutter/material.dart';

class MoreAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppBar appBar;

  const MoreAppBar({Key key, @required this.appBar}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.tealAccent,
      title: Text(
        "More",
        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
      ),
      centerTitle: true,
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
}
