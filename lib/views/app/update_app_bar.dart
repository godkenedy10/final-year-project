import 'package:flutter/material.dart';

class UpdateAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppBar appBar;

  const UpdateAppBar({Key key, @required this.appBar}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
       backgroundColor: Colors.tealAccent,
      title: Text(
        "Updates",
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
      ),
      centerTitle: true,
      
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
}
