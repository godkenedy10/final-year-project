import 'package:flutter/material.dart';
import 'package:fyp/views/components/tiles/my_home_list_tile.dart';
import 'package:fyp/views/components/tiles/saved_home_list_tiles.dart';

class MyHomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemBuilder: (context, index) {
          return MyHomeListTile();
        },
        itemCount: 5);
  }
}
