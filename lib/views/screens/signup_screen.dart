import 'dart:ui';

import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fyp/views/pages/home_page.dart';
import 'package:fyp/views/screens/welcome_screen.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();

  final FocusNode _emailFocusNode = FocusNode();

  final FocusNode _passwordFocusNode = FocusNode();
  final FocusNode _confirmPasswordFocusNode = FocusNode();

  final TextEditingController _emailTextEditingController =
      TextEditingController();

  final TextEditingController _passwordTextEditingController =
      TextEditingController();
  final TextEditingController _confirmPasswordTextEditingController =
      TextEditingController();

  String _ongrouped = "male";

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      decoration: BoxDecoration(
        image: DecorationImage(
            colorFilter: ColorFilter.mode(Colors.grey, BlendMode.saturation),
            image: AssetImage('assets/images/house3.jpg'),
            fit: BoxFit.cover),
      ),
      child: BackdropFilter(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            leading: InkWell(
                onTap: () {
                  Navigator.pop(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                    return WelcomeScreen();
                  }));
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                )),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  color: Colors.transparent,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: RaisedButton(
                          child: ListTile(
                            leading: Icon(
                              FontAwesomeIcons.facebookF,
                              color: Colors.white,
                            ),
                            title: Text(
                              'Sign up with Facebook',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ),
                          color: Colors.blue[900],
                          onPressed: () {},
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                InkWell(
                  onTap: () {},
                  child: Text(
                    'or with email',
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Form(
                    child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          left: 10, right: 10, top: 2, bottom: 5),
                      child: TextFormField(
                        focusNode: _emailFocusNode,
                        controller: _emailTextEditingController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            hintText: 'Email',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(40))),
                      ),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          left: 40, right: 40, top: 5, bottom: 5),
                      child: TextFormField(
                        focusNode: _emailFocusNode,
                        controller: _emailTextEditingController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            hintText: 'Enter username',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(40))),
                      ),
                    ),
                    SizedBox(
                      height: 1,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          left: 40, right: 40, top: 5, bottom: 5),
                      child: TextFormField(
                        focusNode: _emailFocusNode,
                        controller: _emailTextEditingController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            hintText: 'Enter Password',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(40))),
                      ),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          left: 40, right: 40, top: 5, bottom: 5),
                      child: TextFormField(
                        focusNode: _emailFocusNode,
                        controller: _emailTextEditingController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            hintText: 'Confirm password',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(40))),
                      ),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          left: 40, right: 40, top: 5, bottom: 5),
                      child: TextFormField(
                        focusNode: _emailFocusNode,
                        controller: _emailTextEditingController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            hintText: 'Date of Birth',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(40))),
                      ),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Row(
                        children: [
                          Text("MALE"),
                          Radio(
                              value: "male",
                              groupValue: _ongrouped,
                              onChanged: _onradiochange),
                          Text("FEMALE"),
                          Radio(
                              value: "female",
                              groupValue: _ongrouped,
                              onChanged: _onradiochange),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                  ],
                ))
              ],
            ),
          ),
          bottomNavigationBar: BottomAppBar(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    color: Colors.tealAccent,
                    child: FlatButton(
                      child: Text(
                        'SIGN UP',
                        style: TextStyle(fontSize: 20, color: Colors.black),
                      ),
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return HomePage();
                        }));
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        filter: ImageFilter.blur(sigmaX: 5, sigmaY: 8),
      ),
    );
  }

  _onradiochange(sex) {
    print(sex);
    setState(() {
      _ongrouped = sex;
    });
  }
}
