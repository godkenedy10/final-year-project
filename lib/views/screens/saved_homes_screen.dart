import 'package:flutter/material.dart';
import 'package:fyp/views/components/tiles/saved_home_list_tiles.dart';

class SavedHomesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemBuilder: (context, index) {
          return SavedHomeListTile();
        },
        itemCount: 10);
  }
}
