import 'package:flutter/material.dart';
import 'package:fyp/views/components/tiles/update_list_tiles.dart';

class UpdateScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemBuilder: (context, index) {
          return UpdateListTile();
        },
        itemCount: 10);
  }
}
