import 'package:flutter/material.dart';
import 'package:fyp/models/pictures.dart';
import 'package:fyp/views/components/tiles/picture_list_tile.dart';
import 'package:fyp/views/components/tiles/request_list_tile.dart';
import 'package:fyp/views/components/tiles/update_list_tiles.dart';

class PictureScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 65),
            child: InkWell(
                onTap: () {},
                child: Icon(
                  Icons.message,
                  color: Colors.black87,
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 45),
            child: InkWell(
                onTap: () {},
                child: Icon(
                  Icons.favorite,
                  color: Colors.black87,
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 45),
            child: InkWell(
                onTap: () {},
                child: Icon(
                  Icons.share,
                  color: Colors.black87,
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 45),
            child: InkWell(
                onTap: () {},
                child: Icon(
                  Icons.more_horiz,
                  color: Colors.black87,
                  size: 30,
                )),
          ),
        ],
      ),
      body: ListView.builder(
            itemBuilder: (context, index) {
              return PictureListtile(
                imagename: pictures[index],
              );
            },
            itemCount: pictures.length),
      
      bottomSheet: DraggableScrollableSheet(
          initialChildSize: 0.4,
          maxChildSize: 0.8,
          minChildSize: 0.3,
          builder: (context, scrollController) {
            return Container(
                color: Colors.tealAccent,
                child: ListView.builder(
                    itemCount: 1,
                    controller: scrollController,
                    itemBuilder: (BuildContext context, int index) {
                      return RequestListTile();
                    }));
          }),
    );
  }
}
