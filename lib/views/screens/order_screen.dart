import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fyp/views/components/tiles/order_list_tile.dart';

class OrderScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            colorFilter: ColorFilter.mode(Colors.grey, BlendMode.saturation),
            image: AssetImage('assets/images/house1.jpg'),
            fit: BoxFit.cover),
      ),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 5, sigmaY: 8),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            centerTitle: true,
            elevation: 0,
            backgroundColor: Colors.transparent,
            title: Text(
              "ORDERS",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  
                  fontWeight: FontWeight.bold),
            ),
          ),
          body: Column(
            children: [
              Container(
                height: 600,
                color: Colors.transparent,
                child: ListView.builder(
                    itemCount: 1,
                    itemBuilder: (BuildContext context, int index) {
                      return OrderListTile();
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
