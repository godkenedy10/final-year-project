import 'dart:ui';


import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fyp/views/pages/home_page.dart';
import 'package:fyp/views/screens/welcome_screen.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      decoration: BoxDecoration(
        image: DecorationImage(
            colorFilter: ColorFilter.mode(Colors.grey, BlendMode.saturation),
            image: AssetImage('assets/images/house1.jpg'),
            fit: BoxFit.cover),
      ),
      child: BackdropFilter(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
              elevation: 0,
              backgroundColor: Colors.transparent,
              leading: InkWell(
                onTap: () {
                  Navigator.pop(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                    return WelcomeScreen();
                  }));
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                ),
              ),
         
              ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  color: Colors.transparent,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: RaisedButton(
                          child: ListTile(
                            leading: Icon(
                              FontAwesomeIcons.facebookF,
                              color: Colors.white,
                            ),
                            title: InkWell(
                              onTap: () {},
                              child: Text(
                                'Log in with Facebook',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ),
                          ),
                          color: Colors.blue[900],
                          onPressed: () {},
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'or',
                  style:TextStyle(fontSize: 16, color: Colors.black),
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                    child: Column(
                  children: <Widget>[
                    FlatButton(
                      color: Colors.white30,
                      child: TextFormField(
                        decoration: InputDecoration(
                          prefixIcon:Icon(FontAwesomeIcons.user),
                          labelText: ('Username'),
                        ),
                      ),
                      onPressed: () {},
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    FlatButton(
                      color: Colors.white30,
                      child: TextFormField(
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.lock),
                          labelText: ('Password'),
                        ),
                      ),
                      onPressed: () {},
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    InkWell(
                      onTap: (){},
                      child: Text('Forgot your password?'))
                  ],
                ))
              ],
            ),
          ),
          bottomNavigationBar: BottomAppBar(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    color: Colors.tealAccent,
                    child: FlatButton(
                      child: Text(
                        'LOGIN',
                        style: TextStyle(
                            fontSize: 20, color: Colors.black),
                      ),
                      onPressed: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    HomePage()));
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        filter: ImageFilter.blur(sigmaX: 5, sigmaY: 8),
      ),
    );
  }
}
