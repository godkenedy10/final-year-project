import 'package:flutter/material.dart';

class MapScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15,top: 25),
                child: Row(
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      child: Icon(Icons.share,size: 40,),
                      decoration:
                          BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.tealAccent,),
                     
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Text("Search",style: TextStyle(fontSize: 20),),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 180),
                      child: Icon(Icons.more_vert,size: 40,),
                    ),
                  ],
                ),
              )
            ],
          ),
          ListView(),
        ],
      ),
    );
  }
}
