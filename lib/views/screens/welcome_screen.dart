import 'dart:ui';


import 'package:flutter/material.dart';
import 'package:fyp/views/screens/login_screen.dart';
import 'package:fyp/views/screens/signup_screen.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      decoration: BoxDecoration(
        image: DecorationImage(
            colorFilter: ColorFilter.mode(Colors.grey, BlendMode.saturation),
            image: AssetImage('assets/images/house1.jpg'),
            fit: BoxFit.cover),
      ),
      child: BackdropFilter(
          child: Scaffold(
              backgroundColor: Colors.transparent,
              body: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                
                  Spacer(),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 70,
                          color: Colors.tealAccent,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10, top: 10),
                            child: Text(
                              'REAL ESTATE BUSINESS MANAGEMENT SYSTEM',
                              style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),maxLines: 2,
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10, top: 10),
                            child: Text('Using Smart Contract',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w300,
                                    color: Colors.black)),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Spacer()
                ],
              ),
              bottomNavigationBar: BottomAppBar(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        color: Colors.white,
                        child: FlatButton(
                          child: Text('LOGIN'),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(
                                builder: (BuildContext context) {
                              return LoginScreen();
                            }));
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        color: Colors.tealAccent,
                        child: FlatButton(
                          child: Text('SIGNUP'),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(
                                builder: (BuildContext context) {
                              return SignUpScreen();
                            }));
                          },
                        ),
                      ),
                    )
                  ],
                ),
              )),
          filter:ImageFilter.blur(sigmaX: 10, sigmaY: 10)),
    );
  }
}
