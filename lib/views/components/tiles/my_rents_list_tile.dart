import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fyp/views/screens/more_screen.dart';
import 'package:fyp/views/screens/picture_screen.dart';

class MyRentsListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.tealAccent,
          leading: InkWell(
              onTap: () {
                Navigator.pop(context,
                    MaterialPageRoute(builder: (BuildContext context) {
                  return MoreScreen();
                }));
              },
              child: Icon(Icons.arrow_back_ios)),
          title: Text("My Rents"),
          centerTitle: true,
        ),
        body: Card(
          child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      PictureScreen()),
                            );
                          },
                          child: Container(
                            height: 150,
                            width: MediaQuery.of(context).size.width,
                            color: Colors.red,
                            child: Image(image: AssetImage("assets/images/house13.jpg"),fit: BoxFit.cover,),
                          ),
                        ),
                        Positioned(
                            right: 0,
                            child: IconButton(
                                icon: Icon(Icons.favorite), onPressed: () {})),
                        Positioned(
                          bottom: 0,
                          child: Container(
                              padding: EdgeInsets.all(4.0),
                              color: Colors.black54,
                              child: Text(
                                'Rented 1 year ago',
                                style: TextStyle(color: Colors.white),
                              )),
                        )
                      ],
                    ),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'TZS 200,000 /mo',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            '41 Mikocheni Street',
                            style: TextStyle(color: Colors.grey),
                          ),
                          Text(
                            'Kinondoni,DSM',
                            style: TextStyle(color: Colors.grey),
                          ),
                          Text(
                            '4 bds|2 ba|1456 sqft',
                            style: TextStyle(color: Colors.grey),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Container(
                              height: 90,
                              decoration: BoxDecoration(
                                  color: Colors.black12,
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10),
                                      bottomRight: Radius.circular(10))),
                              child: Stack(children: <Widget>[
                                Center(
                                  child: ListTile(
                                    title: Padding(
                                      padding: const EdgeInsets.only(left: 100),
                                      child: Text(
                                        'Rate the Property',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                    subtitle: Padding(
                                      padding: const EdgeInsets.only(left: 50),
                                      child: (RatingBar(
                                        initialRating: 2,
                                        minRating: 1,
                                        direction: Axis.horizontal,
                                        allowHalfRating: true,
                                        itemCount: 5,
                                        itemPadding: EdgeInsets.symmetric(
                                            horizontal: 4.0),
                                        itemBuilder: (context, _) =>
                                            Icon(Icons.star, color: Colors.red),
                                        onRatingUpdate: (rating) {
                                          print(rating);
                                        },
                                      )),
                                    ),
                                  ),
                                ),
                              ]))
                        ]),
                  ])),
        ));
  }
}
