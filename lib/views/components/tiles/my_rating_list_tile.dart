import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fyp/views/components/tiles/more_list_tile.dart';
import 'package:platform_alert_dialog/platform_alert_dialog.dart';

class MyRatingListTiles extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.tealAccent,
        leading: InkWell(
            onTap: () {
              Navigator.pop(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                return MoreListTile();
              }));
            },
            child: Icon(Icons.arrow_back_ios)),
        title: Text("My Ratings"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 350,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              color: Colors.black54, borderRadius: BorderRadius.circular(20)),
          child: Column(
            children: [
              SizedBox(height: 10),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: CircleAvatar(
                      radius: 50,
                      backgroundImage: AssetImage("assets/images/amir.jpg"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: RichText(
                      text: TextSpan(
                          text: "Christian Kennedy",
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w500),
                          children: <TextSpan>[
                            TextSpan(
                                text: "\nRated you on, May 2019",
                                style: TextStyle(color: Colors.grey))
                          ]),
                    ),
                  ),
                  SizedBox(
                    width: 80,
                  ),
                  InkWell(
                    onTap: () {
                      showDialog<void>(
                        context: context,
                        builder: (BuildContext context) {
                          return PlatformAlertDialog(
                            title: Text('Comment'),
                            content: SingleChildScrollView(
                              child: ListBody(
                                children: <Widget>[
                                  Text(
                                      'Your level of property management was perfect'),
                                  Text('we invite you next time'),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    },
                    child: Icon(
                      Icons.comment,
                      size: 30,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 100,
                width: 300,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.black38,
                ),
                child: ListTile(
                    leading: Icon(
                      Icons.location_on,
                      color: Colors.white,
                      size: 30,
                    ),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Oysterbay,Kinondoni",
                          maxLines: 2,
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 20),
                        ),
                        Text(
                          "Al Hassani Mwinyi,high way",
                          maxLines: 2,
                          style: TextStyle(color: Colors.white),
                        ),
                        Text(
                          "no.32, Wazee street",
                          maxLines: 2,
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    )),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  height: 100,
                  width: 350,
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.circular(20)),
                  child: Stack(children: <Widget>[
                    Center(
                      child: ListTile(
                        title: Padding(
                          padding: const EdgeInsets.only(left: 100),
                          child: Text(
                            'Rated as',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                        subtitle: Padding(
                          padding: const EdgeInsets.only(left: 50),
                          child: (RatingBar(
                            initialRating: 2,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                            itemBuilder: (context, _) =>
                                Icon(Icons.star, color: Colors.red),
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                          )),
                        ),
                      ),
                    ),
                  ]))
            ],
          ),
        ),
      ),
    );
  }
}
