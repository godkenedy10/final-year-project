import 'package:flutter/material.dart';

import 'package:fyp/views/screens/picture_screen.dart';

class UpdateListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.only(bottom: 5),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => PictureScreen()),
                      );
                    },
                    child: Container(
                      height: 150,
                      width: MediaQuery.of(context).size.width,
                      child: Image(
                        image: AssetImage("assets/images/house12.jpg"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                      right: 0,
                      child: IconButton(
                          icon: Icon(Icons.favorite), onPressed: () {})),
                  Positioned(
                    bottom: 0,
                    child: Container(
                        padding: EdgeInsets.all(4.0),
                        color: Colors.black54,
                        child: Text(
                          'Listed on 10/2020',
                          style: TextStyle(color: Colors.white),
                        )),
                  )
                ],
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'TZS 1,2000 /month',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      '41 Mikocheni Street',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      'Kinondoni,DSM',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      '4 bds|2 ba|1456 sqft',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      'House for rent',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ])
            ]),
      ),
    );
  }
}
