import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fyp/views/screens/order_screen.dart';
import 'package:platform_alert_dialog/platform_alert_dialog.dart';

class RequestListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 230,
          width: MediaQuery.of(context).size.width,
          color: Colors.transparent,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15, top: 10),
                child: Container(
                  color: Colors.transparent,
                  child: Column(
                    children: [
                      RichText(
                        text: TextSpan(
                            text: "TZS 500,000 /mo",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                                color: Colors.black),
                            children: <TextSpan>[
                              TextSpan(
                                  text: " 2bd|4ba|782 sqft",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.black))
                            ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 30),
                        child: Text("23 Warioba St,Kinondoni, Dar es Salaam"),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: CircleAvatar(
                        radius: 5,
                        backgroundColor: Colors.red,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      "House for Rent |",
                      style: TextStyle(fontSize: 17),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Text(
                  "Located less than 10 minutes from Mwaikibaki road,in Mikocheni.With local schools ,restaurants and religuos houses.This space is great stay with the atmosphere safety and peace of a home setting in a busy city.3 bedrooms,bathrooms 5 with a 1 public toilet, huge sitting room,dinning room and balcons,swimming pool,fenced with large parking space and full time security.UTILITIES included are trash,water(DAWASCO),private LUKU services",
                  maxLines: 8,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 260),
          child: Text(
            "Request Tour",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        Divider(
          color: Colors.black54,
        ),
        Container(
          color: Colors.transparent,
          height: 75,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 280),
                child: Text(
                  "TODAY,JUNE 23",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 15,
                  ),
                  InkWell(
                    onTap: () {},
                    child: Container(
                      height: 40,
                      width: 100,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30)),
                      child: Center(
                          child: Text(
                        "Morning",
                        style: TextStyle(color: Colors.black),
                      )),
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  InkWell(
                    onTap: () {},
                    child: Container(
                      height: 40,
                      width: 100,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30)),
                      child: Center(
                          child: Text(
                        "Afternoon",
                        style: TextStyle(color: Colors.black),
                      )),
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  InkWell(
                    onTap: () {},
                    child: Container(
                      height: 40,
                      width: 100,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30)),
                      child: Center(
                          child: Text(
                        "Evening",
                        style: TextStyle(color: Colors.black),
                      )),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        Container(
          color: Colors.transparent,
          height: 75,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 240),
                child: Text(
                  "TOMORROW,JUNE 24",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 15,
                  ),
                  InkWell(
                    onTap: () {},
                    child: Container(
                      height: 40,
                      width: 100,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30)),
                      child: Center(
                          child: Text(
                        "Morning",
                        style: TextStyle(color: Colors.black),
                      )),
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  InkWell(
                    onTap: () {},
                    child: Container(
                      height: 40,
                      width: 100,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30)),
                      child: Center(
                          child: Text(
                        "Afternoon",
                        style: TextStyle(color: Colors.black),
                      )),
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  InkWell(
                    onTap: () {},
                    child: Container(
                      height: 40,
                      width: 100,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30)),
                      child: Center(
                          child: Text(
                        "Evening",
                        style: TextStyle(color: Colors.black),
                      )),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        Container(
          color: Colors.transparent,
          height: 400,
          child: Column(
            children: [
              Container(
                height: 55,
                padding:
                    EdgeInsets.only(left: 40, right: 40, top: 5, bottom: 5),
                child: TextFormField(
                  // focusNode: _emailFocusNode,
                  // controller: _emailTextEditingController,
                  // keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      hintText: 'First and last name',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
              ),
              Container(
                height: 55,
                padding:
                    EdgeInsets.only(left: 40, right: 40, top: 5, bottom: 5),
                child: TextFormField(
                  // focusNode: _emailFocusNode,
                  // controller: _emailTextEditingController,
                  // keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      hintText: 'Phone number',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
              ),
              Container(
                height: 55,
                padding:
                    EdgeInsets.only(left: 40, right: 40, top: 5, bottom: 5),
                child: TextFormField(
                  // focusNode: _emailFocusNode,
                  // controller: _emailTextEditingController,
                  // keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      hintText: 'example@example.com',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
              ),
              Container(
                height: 125,
                padding:
                    EdgeInsets.only(left: 40, right: 40, top: 5, bottom: 0),
                child: TextFormField(
                  // focusNode: _emailFocusNode,
                  // controller: _emailTextEditingController,
                  // keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      hintText: 'ADD A MESSAGE',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 35, top: 0, right: 35, bottom: 8),
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    height: 40,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.white,
                    ),
                    child: Center(child: Text("Send tour request")),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "HUSSEIN MASASI",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text("Property Owner"),
                      ],
                    ),
                    SizedBox(
                      width: 220,
                    ),
                    InkWell(
                      onTap: () {
                        showDialog<void>(
                          context: context,
                          builder: (BuildContext context) {
                            return PlatformAlertDialog(
                              title: Text(
                                'MOBILE NUMBER',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              content: SingleChildScrollView(
                                child: ListBody(
                                  children: <Widget>[
                                    Text('+255 629044051'),
                                  ],
                                ),
                              ),
                              actions: <Widget>[
                                PlatformDialogAction(
                                  child: Text('Cancel'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                InkWell(
                                  onTap: () {},
                                  child: PlatformDialogAction(
                                    child: Text('Call'),
                                    actionType: ActionType.Preferred,
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                              ],
                            );
                          },
                        );
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.black, width: 2)),
                        child: CircleAvatar(
                          radius: 25,
                          child: Icon(
                            Icons.call,
                            size: 30,
                            color: Colors.black,
                          ),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        RaisedButton(
            color: Colors.white,
            onPressed: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => OrderScreen()));
            },
            child: Text(
              "MAKE AN ORDER",
              style: TextStyle(fontWeight: FontWeight.bold),
            ))
      ],
    );
  }
}
