import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fyp/views/components/tiles/my_purchase_list_tile.dart';
import 'package:fyp/views/components/tiles/my_rating_list_tile.dart';
import 'package:fyp/views/components/tiles/my_rents_list_tile.dart';
import 'package:fyp/views/screens/welcome_screen.dart';

class MoreListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 800,
      color: Colors.transparent,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 30, right: 280),
            child: CircleAvatar(
              radius: 50,
              backgroundImage: AssetImage("assets/images/amir.jpg"),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
              height: 60,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20), color: Colors.black26),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text(
                      "JUMA AWADHI",
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                  ),
                  SizedBox(
                    width: 190,
                  ),
                  InkWell(
                      onTap: () {},
                      child: Container(
                        height: 40,
                        width: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white54,
                        ),
                        child: Icon(
                          Icons.edit,
                          size: 25,
                        ),
                      )),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: InkWell(
              onTap: () {
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => MyRentsListTile()));
              },
              child: Row(
                children: [
                  Icon(Icons.view_list),
                  SizedBox(
                    width: 10,
                  ),
                  Text("My Rents"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: InkWell(
              onTap: () {
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            MyPurchaseListTile()));
              },
              child: Row(
                children: [
                  Icon(Icons.add_shopping_cart),
                  SizedBox(
                    width: 10,
                  ),
                  Text("My Purchase"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: InkWell(
              onTap: () {},
              child: Row(
                children: [
                  Icon(Icons.graphic_eq),
                  SizedBox(
                    width: 10,
                  ),
                  Text("View Insights"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: InkWell(
              onTap: () {},
              child: Row(
                children: [
                  Icon(Icons.report),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Generate Report"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: InkWell(
              onTap: () {
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            MyRatingListTiles()));
              },
              child: Row(
                children: [
                  Icon(Icons.star),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Ratings"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: InkWell(
              onTap: () {},
              child: Row(
                children: [
                  Icon(Icons.notifications),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Notifications"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: InkWell(
              onTap: () {
                Navigator.pop(context,
                    MaterialPageRoute(builder: (BuildContext context) {
                  return WelcomeScreen();
                }));
              },
              child: Row(
                children: [
                  Icon(Icons.lock),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Logout"),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
