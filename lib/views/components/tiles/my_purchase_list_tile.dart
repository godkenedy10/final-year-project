import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fyp/views/screens/more_screen.dart';
import 'package:fyp/views/screens/picture_screen.dart';

class MyPurchaseListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.tealAccent,
          leading: InkWell(
              onTap: () {
                Navigator.pop(context,
                    MaterialPageRoute(builder: (BuildContext context) {
                  return MoreScreen();
                }));
              },
              child: Icon(Icons.arrow_back_ios)),
          title: Text("My Purchases"),
          centerTitle: true,
        ),
        body: Card(
          child: Container(
              padding: EdgeInsets.only(bottom: 5),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      PictureScreen()),
                            );
                          },
                          child: Container(
                            height: 150,
                            width: MediaQuery.of(context).size.width,
                            color: Colors.blue,
                            child: Image(
                              image: AssetImage("assets/images/house16.jpg"),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Positioned(
                          right: 0,
                          child: Container(
                              padding: EdgeInsets.all(4.0),
                              color: Colors.black54,
                              child: Text(
                                'Purchased 2 years ago',
                                style: TextStyle(color: Colors.white),
                              )),
                        )
                      ],
                    ),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'TZS 500,000,000',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            '30 Masaki Street',
                            style: TextStyle(color: Colors.grey),
                          ),
                          Text(
                            'Kinondoni,DSM',
                            style: TextStyle(color: Colors.grey),
                          ),
                          Text(
                            '5 bds|4 ba|1700 sqft',
                            style: TextStyle(color: Colors.grey),
                          ),
                        ]),
                  ])),
        ));
  }
}
