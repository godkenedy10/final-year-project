import 'package:flutter/material.dart';
import 'package:fyp/views/screens/contract_review_screen.dart';


import 'package:fyp/views/screens/picture_screen.dart';

class OrderListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.only(bottom: 5),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => PictureScreen()),
                      );
                    },
                    child: Container(
                      height: 150,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.red,
                    ),
                  ),
                  Positioned(
                      right: 0,
                      child: IconButton(
                          icon: Icon(Icons.favorite), onPressed: () {})),
                  Positioned(
                    left: 0,
                    child: Container(
                        padding: EdgeInsets.all(4.0),
                        color: Colors.black54,
                        child: Text(
                          'Ordered on 09/2020',
                          style: TextStyle(color: Colors.white),
                        )),
                  )
                ],
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'TZS 1,2000 /mo',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      '41 Mikocheni Street',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      'Kinondoni,DSM',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      '4 bds|2 ba|1456 sqft',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      'House for rent',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ]),
            RaisedButton(
                 color: Colors.white30,
                      onPressed: () {
                          Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => ContractReviewScreen()),
                      );
                      },
                      child: Text(
                        "RENT",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
             
            ]),
      ),
    );
  }
}
