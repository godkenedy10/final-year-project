import 'package:flutter/material.dart';
import 'package:fyp/views/screens/picture_screen.dart';

class SavedHomeListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => PictureScreen()),
          );
        },
        child: Container(
          padding: EdgeInsets.only(bottom: 5),
          child: Row(children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 150,
                  width: 150,
                  color: Colors.red,
                  child: Image(image: AssetImage("assets/images/house19.jpg"),fit: BoxFit.cover,),
                ),
                Positioned(
                    right: 0,
                    child: IconButton(
                        icon: Icon(Icons.favorite), onPressed: () {})),
                Positioned(
                  bottom: 0,
                  child: Container(
                      padding: EdgeInsets.all(4.0),
                      color: Colors.black54,
                      child: Text(
                        '137 days ago',
                        style: TextStyle(color: Colors.white),
                      )),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'TZS 1,2000 /month',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      '41 Mikocheni Street',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      'Kinondoni,DSM',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      '4 bds|2 ba|1456 sqft',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      'House for rent',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ]),
            )
          ]),
        ),
      ),
    );
  }
}
