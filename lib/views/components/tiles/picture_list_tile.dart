import 'package:flutter/material.dart';
import 'package:fyp/models/pictures.dart';

class PictureListtile extends StatelessWidget {
  final Picture imagename;

  const PictureListtile({Key key, this.imagename}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5,bottom: 5),
      child: Container(
        height: 150,
        width: MediaQuery.of(context).size.width,
        color: Colors.transparent,
        child: Image.asset(imagename.image,fit: BoxFit.cover,),
      ),
    );
  }
}