import 'package:flutter/material.dart';
import 'package:fyp/views/screens/landlord_review_screen.dart';
import 'package:fyp/views/screens/payment_screen.dart';

class MyHomeListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          child: Container(
            padding: EdgeInsets.only(bottom: 5),
            child: Row(children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: 150,
                    width: 150,
                    child: Image(
                      image: AssetImage("assets/images/house20.jpg"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                    top: 0,
                    child: Container(
                        padding: EdgeInsets.all(4.0),
                        color: Colors.black54,
                        child: Text(
                          '10 days ago',
                          style: TextStyle(color: Colors.white),
                        )),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'TZS 1,2000 /mo',
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        '41 Mikocheni Street',
                        style: TextStyle(color: Colors.grey),
                      ),
                      Text(
                        'Kinondoni,DSM',
                        style: TextStyle(color: Colors.grey),
                      ),
                      Text(
                        '4 bds|2 ba|1456 sqft',
                        style: TextStyle(color: Colors.grey),
                      ),
                      Text(
                        'House for rent',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ]),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 50),
                child: InkWell(
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                LandlordReviewScreen()));
                  },
                  child: Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                        color: Colors.black12,
                        borderRadius: BorderRadius.circular(15)),
                    child: Icon(
                      Icons.edit_attributes,
                      size: 35,
                    ),
                  ),
                ),
              )
            ]),
          ),
        ),
      ],
    );
  }
}
