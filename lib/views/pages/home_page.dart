import 'package:flutter/material.dart';
import 'package:fyp/provider/utility_provider.dart';
import 'package:provider/provider.dart';
import 'package:fyp/views/app/more_app_bar.dart';
import 'package:fyp/views/app/my_home_app_bar.dart';
import 'package:fyp/views/app/saved_homes_app_bar.dart';
import 'package:fyp/views/app/search_app_bar.dart';
import 'package:fyp/views/app/update_app_bar.dart';
import 'package:fyp/views/screens/more_screen.dart';
import 'package:fyp/views/screens/my_home_screen.dart';
import 'package:fyp/views/screens/saved_homes_screen.dart';
import 'package:fyp/views/screens/search_screen.dart';
import 'package:fyp/views/screens/update_screen.dart';

class HomePage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _homeScaffoldKey = new GlobalKey<ScaffoldState>();
  final _screens = <Widget>[
    SearchScreen(),
    UpdateScreen(),
    SavedHomesScreen(),
    MyHomeScreen(),
    MoreScreen()
  ];

  @override
  Widget build(BuildContext context) {
    final _utilityProvider = Provider.of<UtilityProvider>(context);
    _toggleIndex(index) {
      _utilityProvider.setBottomNavigationIndex = index;
    }

    final _appBars = [
      SearchAppBar(
        appBar: AppBar(
          backgroundColor: Colors.tealAccent,
          bottom: PreferredSize(
              child: Container(),
              preferredSize: Size(MediaQuery.of(context).size.width, 50)),
        ),
      ),
      UpdateAppBar(
        appBar: AppBar(
           backgroundColor: Colors.tealAccent,
          bottom: PreferredSize(
              child: Container(),
              preferredSize: Size(MediaQuery.of(context).size.width, 0)),
        ),
      ),
      SavedHomesAppBar(
        appBar: AppBar(
           backgroundColor: Colors.tealAccent,
          bottom: PreferredSize(
              child: Container(),
              preferredSize: Size(MediaQuery.of(context).size.width, 50)),
        ),
      ),
      MyHomeAppBar(
        appBar: AppBar(
           backgroundColor: Colors.tealAccent,
          bottom: PreferredSize(
              child: Container(),
              preferredSize: Size(MediaQuery.of(context).size.width, 0)),
        ),
      ),
      MoreAppBar(
        appBar: AppBar(
           backgroundColor: Colors.tealAccent,
          bottom: PreferredSize(
              child: Container(),
              preferredSize: Size(MediaQuery.of(context).size.width, 0)),
        ),
      )
    ];
    
    return Scaffold(
      appBar: _appBars.elementAt(_utilityProvider.bottomNavigationIndex),
      body: _screens.elementAt(_utilityProvider.bottomNavigationIndex),
      key: _homeScaffoldKey,
      
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _utilityProvider.bottomNavigationIndex,
          onTap: _toggleIndex,
          selectedItemColor: Colors.blue,
          unselectedItemColor: Colors.grey,
          items: [
            BottomNavigationBarItem(
              icon: new Icon(Icons.search),
              title: new Text('Search'),
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.update),
              title: new Text('Updates'),
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.bookmark), title: Text('Saved Homes')),
            BottomNavigationBarItem(
                icon: Icon(Icons.home), title: Text('My Home')),
            BottomNavigationBarItem(
                icon: Icon(Icons.more_horiz), title: Text('More'))
          ]),
      floatingActionButton: _utilityProvider.bottomNavigationIndex == 0
          ? FloatingActionButton(onPressed: () {}, child: Icon(Icons.favorite),backgroundColor: Colors.grey,)
          : Container(),
    );
  }
}
