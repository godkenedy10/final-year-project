
import 'App.dart';
import 'package:flutter/material.dart';
import 'package:fyp/App.dart';
import 'package:provider/provider.dart';
import 'package:fyp/provider/utility_provider.dart';


void main() => runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => UtilityProvider()),
        ],
        child: App(),
      ),
    );
