import 'package:flutter/material.dart';

class UtilityProvider with ChangeNotifier {
  int _bottomNavigationIndex = 0;

  //getter screen indecies
  int get bottomNavigationIndex => _bottomNavigationIndex;

  //setter
  set setBottomNavigationIndex(int index) {
    _bottomNavigationIndex = index;
    notifyListeners();
  }
}
